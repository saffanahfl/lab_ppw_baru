from django.shortcuts import render
from .models import inputStatus
from .models import Subscriber
from .forms import input_form
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
import requests
from django.contrib.auth import logout
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers

# Create your views here.
response = {}
response['counter'] = 0
def index(request) :
	response['status_form'] = input_form
	response['data'] = inputStatus.objects.all()
	html = 'satu.html'
	return render(request, html, response)

def masukin_data(request) :
	form = input_form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['status'] = request.POST['status']
		stat = inputStatus(status=response['status'])
		stat.save()
		return HttpResponseRedirect('../')
	else:
		return HttpResponseRedirect('../')

def tampilprofil(request):
	return render(request, 'profil.html')

def tampiltabel(request):
	return render(request, 'tabel.html')

def datajson(request):
	txt = request.GET.get('find', 'quilting')
	url = "https://www.googleapis.com/books/v1/volumes?q=" + txt
	data = requests.get(url).json()
	return JsonResponse(data)

def pagesubscribe(request):
	return render(request, 'subscribe.html')

def validasi_email(request):
	email = request.GET["email"]
	emails = Subscriber.objects.filter(email=email).all()
	return JsonResponse({"email" : email, "able" : not bool(emails)})

def daftarsubscriber(request):
	if (request.method == "POST") :
		data = dict(request.POST.items())
		subscriber = Subscriber(name = data['name'], email=data['email'], password = data['password'])
		try:
			subscriber.full_clean()
			subscriber.save()
		except Exception as e:
			e = [v[0] for k, v in e.message_dict.items()]
			return JsonResponse({"error": e}, status=400)
		return JsonResponse({"message": "Anda berhasil menjadi subscriber dari halaman ini!"})
	else:
		return render(request, 'subscribe.html', {})

def data_subscriber(request):
	semua = Subscriber.objects.all().values('email', 'name', 'password')
	list_semua = list(semua)
	return JsonResponse(list_semua, safe=False)

def subscriber_list(request):
	return render(request, 'list_subscriber.html')

@csrf_exempt
def add_counter(request):
	if request.user.is_authenticated:
		request.session['counter'] = request.session['counter'] + 1
	else :
		pass
	return HttpResponse(request.session['counter'], content_type= 'application/json')

@csrf_exempt
def min_counter(request):
	if request.user.is_authenticated:
		request.session['counter'] = request.session['counter'] - 1
	return HttpResponse(request.session['counter'], content_type= 'application/json')

def log_out(request):
	request.session.flush()
	logout(request)
	return HttpResponseRedirect('/story11')

def book_per_user(request):
	if request.user.is_authenticated:
		request.session['username'] = request.user.username
		request.session['email'] = request.user.email
		print(dict(request.session))
		if 'counter' not in request.session:
			request.session['counter'] = 0
		response['counter'] = request.session['counter']
		print(dict(request.session))

	else:
		if 'counter' not in request.session:
			request.session['counter'] = 0
		response['counter'] = request.session['counter']

	return render(request, "tabel.html", response)