# Generated by Django 2.1.1 on 2018-11-21 16:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Subscriber',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.CharField(max_length=60)),
                ('name', models.CharField(max_length=60)),
                ('password', models.CharField(max_length=60)),
            ],
        ),
    ]
