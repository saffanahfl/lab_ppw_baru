from django.db import models

# Create your models here.
class inputStatus(models.Model) :
	status = models.CharField(max_length = 300)
	waktu = models.DateTimeField(auto_now_add=True)

class Subscriber(models.Model) :
	email = models.CharField(max_length = 60, unique=True)
	name = models.CharField(max_length = 60)
	password = models.CharField(max_length = 60)