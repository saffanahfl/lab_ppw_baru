function tema1(){
	var bg = document.getElementById('all');
	bg.style.backgroundImage = 'linear-gradient(to right, #8FECDB, #ECE88F)';

	var colorjudul = document.getElementsByClassName('judul');
	for (i = 0 ; i < colorjudul.length ; i++){
		colorjudul[i].style.backgroundColor = '#FCD75D';
	}

	var colorteks = document.getElementsByClassName('teks');
	for (i = 0 ; i < colorjudul.length ; i++){
		colorteks[i].style.backgroundColor = 'white';
	}
}

function tema2(){
	var bg = document.getElementById('all');
	bg.style.backgroundImage = 'linear-gradient(to bottom right, #8FECDA, #D5B9FC)';

	var colorjudul = document.getElementsByClassName('judul');
	for (i = 0 ; i < colorjudul.length ; i++){
		colorjudul[i].style.backgroundColor = '#B09FF0';
	}

	var colorteks = document.getElementsByClassName('teks');
	for (i = 0 ; i < colorjudul.length ; i++){
		colorteks[i].style.backgroundColor = '#D5D8DD';
	}
}