from django.test import TestCase

# Create your tests here.
from django.test import Client
from django.urls import resolve
from .views import index
from .models import inputStatus
from .models import Subscriber
from .views import tampilprofil
from .views import pagesubscribe
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class Story6UnitTest(TestCase):

	def test_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_index_is_exist(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	def test_model_can_create_new_status(self):
		new_status = inputStatus.objects.create(status='mengerjakan lab ppw')
		counting_all_status = inputStatus.objects.all().count()
		self.assertEqual(counting_all_status, 1)

	def test_story6_using_satu_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'satu.html')

	def test_story6_post_success_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/input-data', {'status': test})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)

	def test_story6_post_error_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().get('/input-data', {'status': ''})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)

	def test_challange6_url_exist(self):
		response = Client().get('/profil')
		self.assertEqual(response.status_code, 200)

	def test_challange6_tampilprofil_is_exist(self) :
		found = resolve('/profil')
		self.assertEqual(found.func, tampilprofil)

	def test_challange6_cekHTML(self) :
		response = Client().get('/profil')
		self.assertTemplateUsed(response, 'profil.html')
class Story9UnitTest(TestCase):
	def test_tabelbuku_HTML(self):
		response = Client().get('/story9')
		self.assertTemplateUsed(response, 'tabel.html')

	def test_url_story9_is_exist(self):
		response = Client().get('/story9')
		self.assertEqual(response.status_code, 200)

class Story10UnitTest(TestCase):
	def test_story10_pagesubscribe_is_exist(self) :
		found = resolve('/story10')
		self.assertEqual(found.func, pagesubscribe)

	def test_model_can_create_new_subscriber(self):
		new_status = Subscriber.objects.create(name='abc', email='abc@gmail.com', password='1234')
		counting_all_subscriber = Subscriber.objects.all().count()
		self.assertEqual(counting_all_subscriber, 1)

	def test_story10_using_subscribe_template(self):
		response = Client().get('/story10')
		self.assertTemplateUsed(response, 'subscribe.html')
class Story7FunctionalTest(TestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(Story7FunctionalTest, self).setUp()
	
	def tearDown(self):
		self.selenium.quit()
		super(Story7FunctionalTest, self).tearDown()

	def test_input_todo(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/')

		status = selenium.find_element_by_id('status')
		submit = selenium.find_element_by_id('submit')

		status.send_keys('coba coba')
		submit.send_keys(Keys.RETURN)

		#cek input
		self.assertIn('coba coba', selenium.page_source)

	def test_h1_text(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/')

		h1_text = self.selenium.find_element_by_id('judul').text
		self.assertIn('apa', h1_text)

	def test_title_website(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/')

		self.assertIn('Halo!', self.selenium.title)

	def test_backgroundcolor_body(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/')

		bg_body = selenium.find_element_by_tag_name('body').value_of_css_property('background')
		self.assertEqual(bg_body, 'rgb(247, 243, 188) none repeat scroll 0% 0% / auto padding-box border-box')

	def test_row_atas_color(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/')

		row_size = selenium.find_element_by_id('row_waktu').value_of_css_property('width')
		self.assertEqual(row_size, '173px')

	def test_prestasi_is_in_HTML(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/profil')

		accordion_teks = selenium.find_element_by_tag_name('h4').text
		self.assertIn('Aktivitas', accordion_teks)

