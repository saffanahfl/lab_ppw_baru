from django.conf.urls import url, include
from .views import index
from .views import masukin_data
from .views import tampilprofil
from .views import tampiltabel
from .views import datajson
from .views import pagesubscribe
from .views import daftarsubscriber
from .views import validasi_email
from .views import data_subscriber
from .views import subscriber_list
from django.contrib.auth import views
from .views import log_out
from .views import add_counter
from .views import min_counter
from .views import book_per_user

urlpatterns = [
	url(r'^$', index, name = 'index'),
	url(r'^input-data', masukin_data, name = 'masukin_data'),
	url(r'^profil', tampilprofil, name = 'tampilprofil'),
	url(r'^data-subscriber', data_subscriber, name = 'data-subscriber'),
	url(r'^data', datajson, name = 'datajson'),
	url(r'^story10', pagesubscribe, name = 'pagesubscribe'),
	url(r'^daftar', daftarsubscriber, name = 'daftarsubscriber'),
	url(r'^validasi', validasi_email, name = 'validasi_email'),
	url(r'^tabel-subscriber', subscriber_list, name = 'tabel-subscriber'),
	url(r'^auth/', include('social_django.urls', namespace='social')),
    url(r'^logout', log_out, name = 'logout'),
    url(r'^add_counter', add_counter, name = 'add_counter'),
    url(r'^min_counter', min_counter, name = 'min_counter'),
    url(r'^story11', book_per_user, name = 'book_per_user'),
    url(r'^story9', book_per_user, name = 'tampiltabel'),
]

